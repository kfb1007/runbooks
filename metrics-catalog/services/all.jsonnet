[
  import 'api.jsonnet',
  import 'ci-runners.jsonnet',
  import 'git.jsonnet',
  import 'monitoring.jsonnet',
  import 'registry.jsonnet',
  import 'sidekiq.jsonnet',
  import 'web.jsonnet',
]
